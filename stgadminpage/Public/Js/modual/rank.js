var $grid = $('#bt_rank_grid');
var viewDialog;

context.ready = function() {
    var state = ["待审核","审核通过","审核未通过"];
    $grid.datagrid({
        fit: true,
        border: false,
        url: _ROOT_ + '/index/rank/getData',
        pagination: true,
        columns: [[
            {field: 'game_name', title: '游戏名称', width: 100,formatter:html_encode},
            {field: 'model', title: '模式', width: 100,formatter:html_encode},
            {field: 'type', title: '机型', width: 100,formatter:html_encode},
            {field: 'score', title: '分数', width: 100,formatter:html_encode},
            {field: 'stage', title: '到达位置', width: 100,formatter:html_encode},
            {field: 'user_bid', title: '玩家机签', width: 100,formatter:html_encode},
            {field: 'user_name', title: '玩家名', width: 100,formatter:html_encode},
            {field: 'comment', title: '玩家留言', width: 100,formatter:html_encode},
            {field: 'loaction', title: '玩家位置', width: 100,formatter:html_encode},
            {field: 'source', title: '成绩资源', width: 80,formatter: function(value) {
                return "<a href='"+value+"' target='_blank'>查看</a>";}},
            {field: 'space_name', title: '平台', width: 100,formatter:html_encode},
            {field: 'time', title: '时间', width: 150, align: 'center',formatter:html_encode},
            {field: 'creatTime', title: '创建时间', width: 150, align: 'center'},
            {field: 'status', title: '状态', width: 100, align: 'center', formatter: function(value) {
                return state[value];}},
            {field: 'id', title: '操作', width: 100, align: 'center', formatter: function(value) {
                return '<span title="编辑" class="img-btn icon-edit" type="update" id=' + value + '></span> <span title="删除" class="img-btn icon-remove"  type="delete" id=' + value + '></span>';
            }}
        ]],
        toolbar: [{
            text: '新增',
            iconCls: 'icon-add',
            handler: context.addView
        }],
        onLoadSuccess: function() {
            var $bodyView = $grid.data('datagrid').dc.view2;
            $bodyView.find('span[id]').click(function(e) {
                e.stopPropagation();
                var id = $(this).attr('id');
                var type = $(this).attr('type');
                if (type === 'update') {
                    context.updateView(id);
                } else {
                    context.doDelete(id);
                }
            });
        }
    });

    $('#bt_rank_search_btn').click(function() {
        $grid.datagrid('load', $('#bt_rank_search_from').toJson());
    });
};

context.addView = function() {
    viewDialog = $.dialog({
        title: '新增成绩',
        href: _ROOT_ + '/index/rank/toadd',
        width: 450,
        height: 450,
        bodyStyle: {overflow: 'hidden'},
        buttons: [{
            text: '提交',
            handler: context.doSubmit
        }]
    });
};

context.updateView = function(id) {
    viewDialog = $.dialog({
        title: '编辑成绩',
        href: _ROOT_ + '/index/rank/toUpdate?id=' + id,
        width: 450,
        height: 450,
        bodyStyle: {overflow: 'hidden'},
        buttons: [{
            text: '提交',
            handler: context.doSubmit
        }]
    });
};

context.doDelete = function(id) {
    $.confirm('确认删除？', function(r) {
        if (r) {
            $.post(_ROOT_ + '/index/rank/doDelete', {id: id}, function(rsp) {
                if (rsp.status) {
                    $grid.datagrid('reload');
                } else {
                    $.alert(rsp.msg);
                }
            }, 'JSON');
        }
    });
};

context.doSubmit = function() {
    var $bt_user_from = $('#bt_rank_from');
    if ($bt_user_from.form('validate')) {
        $.post(_ROOT_ + '/index/rank/doSave', $bt_user_from.toJson(), function(rsp) {
            if (rsp.status) {
                $grid.datagrid('reload');
                viewDialog.dialog('close');
            } else {
                $.alert(rsp.msg);
            }
        }, "JSON");
    }
};
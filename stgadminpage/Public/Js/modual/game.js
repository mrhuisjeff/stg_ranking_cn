var $grid = $('#bt_role_grid'), $rigthGrid = $('#bt_role_right_grid'), $userGrid = $('#bt_role_user_grid');
var viewDialog, rid, grantTree, $chooseuserGrid;

context.ready = function() {
    $grid.datagrid({
        fit: true,
        border: false,
        url: _ROOT_ + '/index/game/getData',
        pagination: true,
        columns: [[
                {field: 'name', title: '游戏名称', width: 100},
                {field: 'brand_name', title: '厂商名称', width: 100},
                {field: 'name_en', title: '英文名称', width: 100},
                {field: 'model', title: '游戏模式', width: 150},
                {field: 'type', title: '游戏机体', width: 150},
                {field: 'description', title: '描述', width: 150},
                {field: 'image', title: '图片', width: 100},
                {field: 'source', title: '资源', width: 100},
                {field: 'rank', title: '排序', width: 100},
                {field: 'id', title: '操作', width: 100, align: 'center', formatter: function(value) {
                        return '<span title="编辑" class="img-btn icon-edit" type="update" id=' + value + '></span><span title="删除" class="img-btn icon-remove"  type="delete" id=' + value + '></span>';
                    }}
            ]],
        toolbar: [{
                text: '新增',
                iconCls: 'icon-add',
                handler: context.addView
            }],
        onLoadSuccess: function() {
            var $bodyView = $grid.data('datagrid').dc.view2;
            $bodyView.find('span[id]').click(function(e) {
                e.stopPropagation();
                var id = $(this).attr('id');
                var type = $(this).attr('type');
                if (type === 'update') {
                    context.updateView(id);
                } else {
                    context.doDelete(id);
                }
            });
        }
    });

};

context.addView = function() {
    viewDialog = $.dialog({
        title: '新增游戏',
        href: _ROOT_ + '/index/game/toadd',
        width: 450,
        bodyStyle: {overflow: 'hidden'},
        height: 400,
        buttons: [{
                text: '提交',
                handler: context.doSubmit
            }]
    });
};


context.doSubmit = function() {
    var $bt_role_from = $('#bt_role_from');
    if ($bt_role_from.form('validate')) {
        $.post(_ROOT_ + '/index/game/doSave', $bt_role_from.toJson(), function(rsp) {
            if (rsp.status) {
                $grid.datagrid('reload');
                viewDialog.dialog('close');
            } else {
                $.alert(rsp.msg);
            }
        }, "JSON");
    }
};


context.updateView = function(id) {
    viewDialog = $.dialog({
        title: '编辑游戏',
        href: _ROOT_ + '/index/game/toupdate?id=' + id,
        width: 450,
        bodyStyle: {overflow: 'hidden'},
        height: 400,
        buttons: [{
                text: '提交',
                handler: context.doSubmit
            }]
    });
};

context.doDelete = function(id) {
        $.confirm('确认删除？', function(r) {
            if (r) {
                $.post(_ROOT_ + '/index/game/doDelete', {id: id}, function(rsp) {
                    if (rsp.status) {
                        $grid.datagrid('reload');
                    } else {
                        $.alert(rsp.msg);
                    }
                }, 'JSON');
            }
        });
};

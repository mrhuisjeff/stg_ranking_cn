$(function() {
    var $dialog = $('<div/>');
    var $formBody = $('#form-body');
    $dialog.dialog({
        height: 300,
        width: 500,
        content: $formBody.show(),
        noheader: true,
        buttons: [{
                id: 'loginBtn',
                text: '登陆',
                handler:function(){
                    $.post('index/public/dologin',$formBody.serialize(),function(rsp){
                          if(rsp.status){
                               window.location.reload();
                          }else{
                              $.messager.alert('提示',rsp.msg);
                          }
                    },'JSON').error(function(){
                        $.messager.alert('提示','系统错误！');
                    });
                }
            }]
    });
    $formBody.after($('#logo').show());
    $(window).resize(function() {
        $dialog.dialog('center');
    });
});
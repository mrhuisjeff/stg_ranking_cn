/*
Navicat MySQL Data Transfer

Source Server         : stg
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : stg

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-02-26 14:54:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `mid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `text` varchar(20) NOT NULL,
  `href` varchar(100) DEFAULT NULL,
  `path` varchar(100) NOT NULL,
  `iconCls` varchar(20) DEFAULT NULL,
  `issort` int(1) NOT NULL DEFAULT '0',
  `seq` int(11) NOT NULL DEFAULT '1',
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mid`)
) ENGINE=MyISAM AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('96', '91', '游戏管理', '/game/index', '-90.91.96.', 'icon-role', '0', '3', '0');
INSERT INTO `menu` VALUES ('95', '91', '成绩管理', '/rank/index', '-90.91.95.', 'icon-function', '0', '1', '0');
INSERT INTO `menu` VALUES ('91', '-90', 'stg全国榜管理系统', '', '-90.91.', 'icon-application', '1', '0', '0');
INSERT INTO `menu` VALUES ('97', '91', '玩家管理', '/gamer/index', '-90.91.97.', 'icon-user', '0', '2', '0');
INSERT INTO `menu` VALUES ('98', '91', '厂商管理', '/brand/index', '-90.91.98.', 'icon-department', '0', '4', '0');
INSERT INTO `menu` VALUES ('99', '91', '平台管理', '/space/index', '-90.91.99.', 'icon-dict', '0', '5', '0');

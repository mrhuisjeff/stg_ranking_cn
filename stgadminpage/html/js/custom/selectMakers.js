$(function(){
				
	$.ajax({
	   type: "GET",
	   url: "http://ranking.niyaozao.com/frontapi/api/brand/getData",
	   dataType:"json",
	   jsonp: 'jsoncallback',
	   success: function(res){
	   		var content = "<div id='selectMakersDiv'>";
	   		$.each(res,function(i,obj){
	   			var id = obj.id;
	   			var name = obj.name;
	   			var img = "<img src='"+obj.source+"' onerror=\"this.src='img/makerBG.png'\" />";
	   			var msover = "onmouseover='makerMouseOver(this);'";
	   			var msout = "onmouseout='makerMouseOut(this);'";
	   			var ck = "onclick=\"nowMaker.id = "+id+";nowMaker.name= '"+name+"';toUrl('selectGames.html');\"";
	   			content+="<div "+msover+" "+msout+" "+ck+"><span>"+name+"</span>"+img+"</div>";
	   			makers[i] = {id:id,name:name};
	   		});
	   		content+="</div>"
	   		$("#selectMakerBar").html(content);
	   		$("#selectMakersDiv").css("width",$("#selectMakersDiv div").length*174);
	   		if($("#selectMakersDiv div").length>7){
	   			$("#selectMakerBar").css("overflow-x","auto");
	   		}
	   		initMakerDiv();
	   }
	});
	
});

function initMakerDiv(){
	var liColorNum = 1;
	$.each($("#selectMakersDiv div"),function(i,obj){
		var bgColor = "";
		if(liColorNum==1){bgColor="#00A3E8";}
		else if(liColorNum==2){bgColor="#61BB35";}
		else if(liColorNum==3){bgColor="#F15B00";}
		else if(liColorNum==4){bgColor="#F09200";}
		else if(liColorNum==5){bgColor="#8B59A2";liColorNum=0;};
		$(obj).css("background",bgColor);
		var span = $(obj).find("span");
		var left = $(obj).index()*174-180;
		var top = 325;
		if(isOldIE()){
			left = $(obj).index()*174+7;
			top = 140;
		}
		$(span).css({"left":left,"top":top}).show();
		liColorNum++;
		return;
	});
}

function makerMouseOver(obj){
	var left = $(obj).index()*174;
	var top = 0;
	var img = $(obj).find("img");
	$(img).css({"width":154,"height":1});
	$(img).css({"left":left,"top":top}).show().animate({height:300},100);
}

function makerMouseOut(obj){
	$(obj).find("img").hide();
}


$(function(){
	
	$("#makerName span").html(nowMaker.name);
	
	$("#rankLogo img").attr("src",nowGame.image);
	
	setNameXY();
	
	$.each(spaces,function(i,obj){
		var content = "<option value='"+obj.id+"'>"+obj.name+"</option>";
		$("#gameSpace").append(content);
	});
	
	$.each(nowGame.model.split(","),function(i,modelName){
		var content = "<option value='"+modelName+"'>"+modelName+"</option>";
		$("#gameModel").append(content);
	});
	
	$.each(nowGame.type.split(","),function(i,typeName){
		var content = "<option value='"+typeName+"'>"+typeName+"</option>";
		$("#gameType").append(content);
	});
	
	loadRank();
	
	$("#gameModel,#gameType,#gameSpace").change(function(){
		loadRank();
	});
	
	$("#makerName").click(function(){
		toUrl("selectMakers.html");
	});
	
	$("#rankLogo").click(function(){
		toUrl("selectGames.html");
	});
	
	$(window).resize(function(){
		setNameXY();
    });
	
});

function rankClick(obj){
	var src = $(obj).css("background-image");
	if(src.indexOf("BG1")>-1){
		src = src.replace("BG1","BG2");
		$(obj).animate({height:108},100,null,function(){$(obj).find("td").css("padding","8px 40px 8px 40px");$(obj).find(".rankInfo").show();});
		$(obj).css("background-image",src);
	}else{
		src = src.replace("BG2","BG1");
		$(obj).find("td").css("padding","5px 40px 5px 40px");
		$(obj).find(".rankInfo").hide();
		$(obj).animate({height:34},100,null,function(){$(obj).css("background-image",src);});
	}
}

function loadRank(){
	$.ajax({
	   type: "GET",
	   url: "http://ranking.niyaozao.com/frontapi/api/rank/getData",
	   data: {gid:nowGame.id,type:$("#gameType").val(),model:$("#gameModel").val(),space_ids:$("#gameSpace").val()},
	   dataType:"json",
	   jsonp: 'jsoncallback',
	   success: function(res){
	   		$("#rank").remove();
		   	if(res.length==0){
		   		alert("查询到0条成绩，请检查您的查询条件或者投递新成绩");
		   		return;
		   	}
		   	var content = "<div id='rank'>";
		   	$.each(res, function(i,obj){
		   		var user_bid = obj.user_bid;
		   		var score = obj.score;
		   		var type = obj.type;
		   		var model = obj.model;
		   		var loaction = obj.loaction;
		   		var space_name = obj.space_name;
		   		var comment = obj.comment;
		   		var source = obj.source;
		   		var time = obj.time;
		   		var oclick = " onclick='rankClick(this);' ";
		   		content+="<table"+oclick+">"+
							"<tr>"+
								"<td width='10%'>"+(i+1)+"&nbsp;&nbsp;&nbsp;位</td>"+
								"<td width='16.5%'>"+user_bid+"</td>"+
								"<td width='23%'>"+score+"</td>"+
								"<td width='16.5%'>"+model+"</td>"+
								"<td width='16.5%'>"+type+"</td>"+
								"<td width='16.5%'>"+time+"</td>"+
							"</tr>"+
							"<tbody class='rankInfo'>"+
								"<tr>"+
									"<td colspan=2>玩家区域："+loaction+"</td>"+
									"<td colspan=2>游戏平台："+space_name+"</td>"+
									"<td colspan=2></td>"+
								"</tr>"+
								"<tr><td colspan=6>玩家感言："+comment+"</td></tr>"+
								"<tr><td colspan=6>成绩照片/录像：<a onclick=\"window.open('"+source+"');\">"+source+"</a><div style='height:10px;'></div></td></tr>"+
							"</tbody>"+
						"</table>";
		   	});
		   	content+="</div>";
		   	$("#rankLogo").after(content);
	   }
	});
}

function setNameXY(){
	var left = $("#makerName").offset().left-277;
	var top = $("#makerName").offset().top+225;
	if(isOldIE()){
		left = left+288;
		top = top-283;
	}
	$("#makerName span").css({"left":left,"top":top}).show();
	
	top = $("#contentDiv").offset().top-30;
	$("#rankTitle1").css({"top":top}).show();
		$("#rankOption").css("top",top+590).show();
}

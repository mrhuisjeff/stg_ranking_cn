$(function(){
				
	$.each(makers,function(i,obj){
		var content = "<option value='"+obj.id+"'>"+obj.name+"</option>";
		$("#selectMaker").append(content);
	});
	
	$("#selectMaker").change(function(){
		var mid = this.value;
		games = [];
		$.ajax({
		   type: "GET",
		   url: "http://ranking.niyaozao.com/frontapi/api/game/getData",
		   data: {id:mid},
		   dataType:"json",
		   jsonp: 'jsoncallback',
		   success: function(gameList){
		   		$("#selectGame option").remove();
		   		$("#selectGame").append("<option value='-1'>请选择</option>");
		   		$.each(gameList,function(i,obj){
					var content = "<option value='"+obj.id+"'>"+obj.name+"</option>";
					$("#selectGame").append(content);
					games[i] = {id:obj.id,name:obj.name,model:obj.model,type:obj.type};
				});
		   }
		});
	});
	
	$("#selectGame").change(function(){
		var gid = this.value;
		$.each(games, function(i,obj) {
			if(obj.id == gid){
				var model = obj.model.split(",");
		   		var type = obj.type.split(",");
		   		$("#selectModel option").remove();
		   		$.each(model,function(i,obj){
					var content = "<option value='"+obj+"'>"+obj+"</option>";
					$("#selectModel").append(content);
				});
				$("#selectType option").remove();
				$.each(type,function(i,obj){
					var content = "<option value='"+obj+"'>"+obj+"</option>";
					$("#selectType").append(content);
				});
		   		return;
			}
		});
		$.ajax({
		   type: "GET",
		   url: "http://ranking.niyaozao.com/frontapi/api/space/getData",
		   data: {gid:gid},
		   dataType:"json",
		   jsonp: 'jsoncallback',
		   success: function(spaceList){
		   		$("#selectSpace option").remove();
		   		$.each(spaceList,function(i,obj){
					var content = "<option value='"+obj.id+"'>"+obj.name+"</option>";
					$("#selectSpace").append(content);
				});
		   }
		});
	});
	
	$("#sub").click(function(){
		var mk = $("#selectMaker").val();
		var ge = $("#selectGame").val();
		var ub = $("#user_bid").val();
		var un = $("#user_name").val();
		var sc = $("#score").val();
		var md = $("#selectModel").val();
		var te = $("#selectType").val();
		var sp = $("#selectSpace").val();
		var ct = $("#comment").val();
		var lc = $("#loaction").val();
		var te = $("#time").val();
		var se = $("#source").val();
		if(-1 == mk){
			alert("请选择厂商");
			return;
		}else if(-1 == ge){
			alert("请选择游戏");
			return;
		}else if("" == ub){
			alert("请输入玩家机签");
			return;
		}else if("" == sc){
			alert("请输入分数");
			return;
		}else if(-1 == sp){
			alert("请选择平台");
			return;
		}
		$.ajax({
		   type: "POST",
		   url: "http://ranking.niyaozao.com/frontapi/api/rank/submit",
		   data: {game_id:ge,user_bid:ub,user_name:un,score:sc,model:md,type:te,space:sp,comment:ct,loaction:lc,time:te,source:se},
		   dataType:"json",
		   jsonp: 'jsoncallback',
		   success: function(res){
		   	alert(res.msg)
		   }
		});
	});

});



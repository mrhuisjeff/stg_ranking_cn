$(function(){
	$("#gameName").remove();
	setNameXY();
	$.ajax({
	   type: "GET",
	   url: "http://ranking.niyaozao.com/frontapi/api/game/getData",
	   data: {id:nowMaker.id},
	   dataType:"json",
	   jsonp: 'jsoncallback',
	   success: function(res){
		   	if(res.length==0){
		   		alert("该厂商没有添加任何游戏，如有需要请联系管理员");
		   		return;
		   	}
	   		var content = "<div id='gameName'><ul>";
	   		$.each(res,function(i,obj){
	   			var name = obj.name;
	   			var model = obj.model;
	   			var type = obj.type;
	   			var image = obj.image;
	   			var img = "<img src='"+image+"' onerror=\"this.src='img/gameImg.png'\" />";
	   			var msover = "onmouseover='makerMouseOver(this);'";
	   			var msout = "onmouseout='makerMouseOut(this);'";
	   			var ck = "onclick=\"nowGame.id = "+obj.id+";nowGame.name= '"+name+"';nowGame.model='"+model+"';nowGame.type='"+type+"';nowGame.image='"+image+"';toUrl('rank.html');\"";
	   			content+="<li "+msover+" "+msout+" "+ck+">"+img+"<span>"+name+"</span></li>";
	   		});
	   		content+="</ul></div>";
	   		$("#makerName").after(content);
	   }
	});
	
	$("#makerName").click(function(){toUrl('selectMakers.html');});
	
	$(window).resize(function(){
		setNameXY();
    });
	
});

function makerMouseOver(obj){
	var img = $(obj).find("img");
	$(img).css({"width":1,"height":80});
	$(img).show().animate({width:500},100);
}

function makerMouseOut(obj){
	$(obj).find("img").hide();
}

function setNameXY(){
	var left = $("#makerName").offset().left-276;
	var top = $("#makerName").offset().top+225;
	if(isOldIE()){
		left = left+288;
		top = top-283;
	}
	$("#makerName span").css({"left":left,"top":top}).html(nowMaker.name).show();
}


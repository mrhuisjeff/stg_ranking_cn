$(document).ajaxSend(function(){loading();});
$(document).ajaxComplete(function(){closeLoading();});

var windowsHeight = window.screen.height;

$(function(){
});

function isOldIE(){//如果是IE11以下的IE版本
	if(navigator.userAgent.indexOf('MSIE')>0 && navigator.userAgent.indexOf("Trident/7.0")==-1){
		return true;
	}else{
		return false;
	}
}

function toUrl(url){
	url = url+"?rd="+new Date();
	$.ajax({
		type:'get',
		url:url,
		success:function(res){
			$("#contentDiv").css("height",windowsHeight-250).html(res);
			$(".backDIV").css({"top":$("#contentDiv").offset().top-30}).show();
		}
	});
}

function openCeng(url,width,height,top){
	if(undefined!=$('.mask').attr("class"))return;
	var content = "<div class='mask'></div>"+
				"<div class='cengBG'>"+
						"<center>"+
							"<div id='cengA'></div>"+
							"<div id='cengB'></div>"+
						"</center>"+
					"</div>"+
				"<div class='ceng'>"+
					"<center>"+
						"<div id='closeCeng'><img src='img/delete.png' onclick=\"$('.mask,.ceng,.cengBG').remove();\"/></div>"+
						"<div id='cengContent'></div>"+
					"</center>"+
				"</div>";
	$("body").append(content);
	
	if(undefined!=width)$("#closeCeng,#cengContent,#cengA,#cengB").css("width",width);
	if(undefined!=height)$("#cengContent,#cengB").css("height",height);
	if(undefined!=top){$("#closeCeng").css("margin-top",top);$("#cengA").css("margin-top",top+3);}
	
	$.ajax({
	   type: "GET",
	   url: url,
	   async: false,
	   success: function(res){
	   		$("#cengContent").html(res);
	   }
	});
	$('.mask,.ceng,.cengBG').fadeIn(200);
}

window.alert = function(ct,fun){
	var content = "<div class='alertMask'></div>"+
		"<div class='alertBG'>"+
			"<center>"+
				"<div id='alertA' style='width:500px;margin-top:103px;'></div>"+
				"<div id='alertB' style='height:200px;width:500px;'></div>"+
			"</center>"+
		"</div>"+
		"<div class='alert'>"+
			"<center>"+
				"<div id='closeAlert' style='width:500px;margin-top:100px;'><img src='img/delete.png' onclick=\"if(undefined!=alertFun){alertFun();}alertFun=undefined;$('.alertMask,.alert,.alertBG').remove();\"/></div>"+
				"<div id='alertContent' style='height:200px;width:500px;'>"+
					"<fieldset id='deliveryFD'>"+
					    "<legend>温馨提示</legend>"+
					    "<table width='100%'>"+
					    	"<tr>"+
					    		"<td width=80 ><img src='img/pt.png' width=80 /></td>"+
					    		"<td>"+
					    			"<div id='alertCT' style='height: 60px;overflow-y: auto;margin: 10px;'></div>"+
					    		"</td>"+
					    	"</tr>"+
					    "</table>"+
					"</fieldset>"+
					"<button class='greenBtn' onclick=\"if(undefined!=alertFun){alertFun();}alertFun=undefined;$('.alertMask,.alert,.alertBG').remove();\">确定</button>"+
				"</div>"+
			"</center>"+
		"</div>";
	$("body").append(content);
	alertFun = fun;
	if(undefined==ct || undefined==ct)ct = "温馨提示";
	$("#alertCT").html(ct);
	$('.alertMask,.alert,.alertBG').fadeIn(200);
}
function loading(){
	var content = "<table class='loadingMask'>"+
					"<tr>"+
						"<td valign='middle'><img src='img/loading.gif' width='40'/>Loding......</td>"+
					"</tr>"+
				"</table>";
	$("body").append(content);
	$(".loadingMask").fadeIn(200);
}
function closeLoading(){
	$(".loadingMask").remove();
}

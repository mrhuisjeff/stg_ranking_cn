<?php

/**
 * 角色模型
 *
 */
class GameModel extends Model {

    function delData($id){
        $sql = "UPDATE game SET is_del = 1 where id = %d";
        $data = $this->query($sql, $id);
        return $data;
    }
    function setSpace($id,$ids){
        $sql = "DELETE FROM game_space where g_id = %d";
        $data = $this->execute($sql, $id);
            foreach ($ids as $addId) {
                $sql = "INSERT INTO game_space VALUES (%d,%d)";
                $this->execute($sql,$id,$addId);
            }
        return $data;
    }
    function getRightTreeData() {
        $sql = "select CONCAT('f',fid) as id, text, CONCAT('m',mid) as pid,0 type from " . c('db_prefix') . "functions union select CONCAT('m',mid), text, CONCAT('m',pid),issort from " . c('db_prefix') . "menu";
        return $this->query($sql);
    }
    function getSpace($g_id){
        $sql = "select s.id as id,s.name as name from space s left JOIN game_space gs on gs.s_id = s.id where gs.g_id = %d";
        return $this->query($sql,$g_id);
    }
    function getRightData($rid) {
        $sql = "select rr.id as id, f.text, f.mid as pid,f.fid as tid,1 type from ( select * from " . c('db_prefix') . "role_right rr where rr.rid = %d and rr.type = 1 ) rr join " . c('db_prefix') . "functions f on f.fid = rr.tid union select rr.id, m.text,  m.pid,m.mid as tid,0 from ( select * from " . c('db_prefix') . "role_right rr where rr.rid = %d and rr.type = 0 ) rr join " . c('db_prefix') . "menu m on m.mid = rr.tid";
        return $this->query($sql, $rid, $rid);
    }

    function getadduserData($rid, $condition, $page, $rows) {
        $sql = " SELECT u.uid, u.uname, u.account, u.mail FROM " . c('db_prefix') . "user AS u WHERE NOT EXISTS ( SELECT ru.uid FROM " . c('db_prefix') . "role_user ru WHERE u.uid = ru.uid AND ru.rid = %d ) and 1=1 ";
        $count = " SELECT count(1) c FROM " . c('db_prefix') . "user AS u WHERE NOT EXISTS ( SELECT ru.uid FROM " . c('db_prefix') . "role_user ru WHERE u.uid = ru.uid AND ru.rid = %d ) and 1=1 ";
        $where = '';
        $val = array($rid);
        if (count($condition) > 0) {
            foreach ($condition as $key => $value) {
                $where .='and u.' . $key . ' ' . $value[0] . ' \'%s\' ';
                $val[] = $value[1];
            }
        }
        $count = $this->parseSql($count . $where, $val);
        $sql = $sql . $where . ' limit %d,%d';
        array_push($val, ($page - 1) * $rows);
        array_push($val, $rows);
        $sql = $this->parseSql($sql, $val);
        $rs = $this->query($count);
        $cr = $rs[0];
        return array($this->query($sql), $cr['c']);
    }

}
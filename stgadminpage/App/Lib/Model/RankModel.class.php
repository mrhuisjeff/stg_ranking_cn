<?php


/**
 * FunctionModel
 *
 */
class RankModel extends Model {


    public function getResourcesByUid($uid) {
        if ($uid) {
            $sql = "SELECT f.resources FROM " . C('DB_PREFIX') . "functions f WHERE EXISTS ( SELECT * FROM ( SELECT rr.tid FROM " . C('DB_PREFIX') . "role_right rr WHERE EXISTS ( SELECT rid FROM " . C('DB_PREFIX') . "role_user AS ru WHERE ru.uid = %d AND rr.rid = ru.rid ) AND rr.type = 1 ) ids WHERE ids.tid = f.fid )";
            return $this->query($sql, $uid);
        }
        return null;
    }

}

?>

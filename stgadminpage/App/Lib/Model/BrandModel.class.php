<?php


/**
 * OrgModel
 *
 */
class BrandModel extends Model {

    function getData() {
        $sql = "select id, name,description, rank,source,id as cid from " . c('db_prefix') . "brand where is_del !=1 order by id";
        return $this->query($sql);
    }
    function delData($id){
        $sql = "UPDATE brand SET is_del = 1 where id = %d";
        $data = $this->query($sql, $id);
        return $data;
    }
    function getUidByOid($id) {
        $sql = "select id, name,description, rank,source from " . c('db_prefix') . "brand where id = %d";
        $data = $this->query($sql, $id);
        if (!empty($data) && count($data) == 1) {
            $row = $data[0];
            return $row['uids'];
        }
        return '';
    }

    function getadduserData($rid, $condition, $page, $rows) {
        $sql = " SELECT u.uid, u.uname, u.account, u.mail FROM " . c('db_prefix') . "user AS u WHERE NOT EXISTS ( SELECT ru.uid FROM " . c('db_prefix') . "user_org ru WHERE u.uid = ru.uid AND ru.oid = %d ) and 1=1 ";
        $count = " SELECT count(1) c FROM " . c('db_prefix') . "user AS u WHERE NOT EXISTS ( SELECT ru.uid FROM " . c('db_prefix') . "user_org ru WHERE u.uid = ru.uid AND ru.oid = %d ) and 1=1 ";
        $where = '';
        $val = array($rid);
        if (count($condition) > 0) {
            foreach ($condition as $key => $value) {
                $where .='and u.' . $key . ' ' . $value[0] . ' \'%s\' ';
                $val[] = $value[1];
            }
        }
        $count = $this->parseSql($count . $where, $val);
        $sql = $sql . $where . ' limit %d,%d';
        array_push($val, ($page - 1) * $rows);
        array_push($val, $rows);
        $sql = $this->parseSql($sql, $val);
        $rs = $this->query($count);
        $cr = $rs[0];
        return array($this->query($sql), $cr['c']);
    }
}

?>

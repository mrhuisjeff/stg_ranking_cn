<?php


/**
 * 用户模型
 *
 */
class GamerModel extends Model {
    function delData($id){
        $sql = "UPDATE gamer SET is_del = 1 where id = %d";
        $data = $this->execute($sql, $id);
        return $data;
    }
    protected function createTime() {
        import("@.ORG.Util.Date");
        $date = new Date();
        return $date->format();
    }

    protected function pwdHash() {
        if (isset($_POST['password'])) {
            return pwdHash($_POST['password']);
        } else {
            return false;
        }
    }

    function getUserDataWhitOid($oid, $page, $rows) {
        $sql = "select u.uid, u.uname, u.account, u.mail, om.id from " . c('db_prefix') . "org_manager as om right join " . c('db_prefix') . "user as u on om.uid = u.uid and om.oid = %d order by id desc limit %d,%d";
        
        return $this->query($sql, $oid, ($page-1)*$rows, $rows);
    }

}
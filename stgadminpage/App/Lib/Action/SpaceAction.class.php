<?php


/**
 * DictAction
 *
 */
class SpaceAction extends BaseAction {

    public function index() {
        $this->display();
    }


    public function getData() {
        $Model = M('Space');
        $data = $Model->where("is_del !=1")->page($this->page, $this->rows)->select();
        $this->returnGridData($data, $Model->where("is_del !=1")->count());
    }
    public function getDataNoPage() {
        $Model = M('Space');
        $data = $Model->where("is_del !=1")->select();
        $this->ajaxReturn($data);
    }

    public function comboData() {
        $TypeModel = D('DictType');
        $list = $TypeModel->where(array('isdelete' => 0))->select();
        $this->ajaxReturn($list);
    }

    public function toTypeView() {
        $this->display('type');
    }

    public function toaddView() {
        $this->display('add');
    }

    public function toupdateView() {
        $id = $_GET['id'];

        if ($id) {
            $Model = D("Space");
            $data = $Model->where('id = %d', $id)->find();
            if ($data) {
                $this->assign('data', $data);
            }
        }
        $this->display('add');
    }

    public function getTypeData() {
        $TypeModel = D('DictType');
        $dataList = $TypeModel->order('isdelete asc')->select();
        $this->returnGridData($dataList, $TypeModel->count());
    }

    public function doSaveType() {
        if (isset($_POST["deleted"])) {
            $deleted = stripcslashes($_POST["deleted"]);
            $listDeleted = json_decode($deleted, TRUE);
        }

        if (isset($_POST["inserted"])) {
            $inserted = stripcslashes($_POST["inserted"]);
            $listInserted = json_decode($inserted, TRUE);
        }

        if (isset($_POST["updated"])) {
            $updated = stripcslashes($_POST["updated"]);
            $listUpdated = json_decode($updated, TRUE);
        }

        $TypeModel = D('DictType');

        if (!empty($listDeleted)) {
            foreach ($listDeleted as $value) {
                $TypeModel->where("id = %d", $value['id'])->save(array('isdelete' => 1));
            }
        }

        if (!empty($listUpdated)) {
            foreach ($listUpdated as $value) {
                $TypeModel->save($value);
            }
        }

        if (!empty($listInserted)) {
            $TypeModel->addAll($listInserted);
        }
        $this->returnStatus();
    }

    public function doSave() {
        $Model = D("Space");
        if (!$Model->create()) {
            $this->returnStatus(false, $Model->getError());
        } else {
            if (empty($Model->id)) {
                $Model->add();
            } else {
                $Model->save();
            }

            if ($Model->getError()) {
                $this->returnStatus(false, $Model->getError());
            }
            $this->returnStatus();
        }
    }

    public function doDelete() {
        $id = $_POST['id'];
        $Model = D("Space");
        $Model->delData($id);
        $this->returnStatus();
    }

}

?>

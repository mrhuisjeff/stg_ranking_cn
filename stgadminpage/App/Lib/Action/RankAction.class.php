<?php


/**
 * FunctionAction
 */
class RankAction extends BaseAction {

    public function index() {
        $this->display();
    }

    public function toAdd() {
        $this->display('add');
    }

    public function toUpdate() {
        $id = $_GET['id'];

        if ($id) {
            $Model = D("Rank");
            $data = $Model->where('id = %d', $id)->find();
            if ($data) {
                $this->assign('data', $data);
            }
        }
        $this->display('add');
    }

    public function doSave() {
        $Model = D("Rank");
        if (!$Model->create()) {
            $this->returnStatus(false, $Model->getError());
        } else {
            if (empty($Model->id)) {
                $Model->add();
            } else {
                $Model->save();
            }

            if ($Model->getError()) {
                $this->returnStatus(false, $Model->getError());
            }
            $this->returnStatus();
        }
    }

    public function menuTree() {
        $Menu = M('Menu');
        $dataList = $Menu->order('seq asc')->select();
        $this->ajaxReturn(BuildMenuTree($dataList));
    }

    public function getData() {
        $gId = $_POST["game_id"];
        $status = $_POST["status"];
        $type = $_POST["type"];
        $model = $_POST["model"];
        $search ="";
        if(!empty($gId)){
            $search = $search." and r.game_id = ".$gId;
            if(!empty($type)){
                $search = $search." and r.type = '".$type."'";
            }
            if(!empty($model)){
                $search = $search." and r.model = '".$model."'";
            }
        }
        if(!empty($status) || $status ==="0"){
            $search = $search." and r.status = ".$status;
        }
        $Mode = D('Rank');
        $data = $Mode->table('rank r, game g,space s')->field("r.id,r.score,r.status,r.user_name,r.user_bid,r.comment,r.stage,r.model,r.type,r.loaction,r.source,r.time,r.creatTime,g.name as game_name,s.name as space_name")->where("r.game_id = g.id and s.id = r.space_id".$search)->where($this->condition)->page($this->page, $this->rows)->select();
        Log::write($Mode->getLastSql());
        $this->returnGridData($data, $Mode->where($this->condition)->count());
    }

    public function doDelete() {
        $id = $_POST['id'];

        $Model = D("Rank");
        $Model->where('id = %d', $id)->delete();

        $this->returnStatus();
    }

}

?>

<?php


/**
 * UserAction
 *
 */
class GamerAction extends BaseAction {

    public function index() {
        $this->display();
    }

    public function getDataNoPage() {
        $Model = M('Gamer');
        $data = $Model->where("is_del !=1")->select();
        $this->ajaxReturn($data);
    }
    public function getData() {
        $Mode = D('Gamer');
        $data = $Mode->where("is_del != 1")->where($this->condition)->page($this->page, $this->rows)->order($this->order)->select();
        $this->returnGridData($data, $Mode->where("is_del != 1")->where($this->condition)->count());
    }

    public function toAdd() {
        $Role = D('Role');
        $roles = $Role->field('rid,text')->where(array('status' => 0))->select();
        $this->assign('roles', json_encode($roles));
        $this->display('add');
    }

    public function doSave() {
        $Model = D("Gamer");
        if (!$Model->create()) {
            $this->returnStatus(false, $Model->getError());
        } else {
            $Model->startTrans();

            if (empty($Model->id)) {
                $result = $Model->add();
            } else {
                $result = $Model->save();
            }


            $Model->commit();
            $this->returnStatus();
        }
    }

    public function doDelete() {
        $id = $_POST['id'];
        $Model = D("Gamer");
        $Model->delData($id);
        $this->returnStatus();
    }

    public function toUpdate() {
        $uid = $_GET['id'];
        if ($uid) {
            $Model = D("Gamer");
            $data = $Model->where('id = %d', $uid)->find();
            if ($data) {
                $this->assign('data',$data );
            }
        }
        $this->display('add');
    }

}

?>

<?php if (!defined('THINK_PATH')) exit();?><form id="bt_rank_from" class="form">
    <table align="center">
        <tr>
            <td>状态：</td>
            <td><select id="status_combo" class="easyui-combobox" name="status" style="width:130px;"
                        data-options="panelHeight:'auto',editable:false,value:'<?php echo ($data["status"]); ?>'">
                <option <?php echo ($data['status']==1?'selected':''); ?> value="1">审核通过</option><option <?php echo ($data['status']==2?'selected':''); ?> value="2">审核未通过</option><option value="0" <?php echo ($data['status']==='0'?'selected':''); ?>>待审核</option>
            </select>
            </td>
        </tr>
        <tr>
            <td>选择游戏：</td>
            <td colspan="3">
            <select id="game_combo" class="easyui-combobox easyui-validatebox" required="required" name="game_id" style="width:130px;"
                        data-options="
                valueField:'id',
                panelHeight:'auto',
                value:'<?php echo ($data["game_id"]); ?>',
                textField:'name',
                url:'__ROOT__/index/game/getDataNoPage',
                onLoadSuccess:function(data){
                    var key = '<?php echo ($data["game_id"]); ?>';
                    if(key!=''){
                        var thisData;
                    for(var i=0,length=data.length;i<length;i++){
                        if(key==data[i].id){
                            thisData=data[i];
                            break;
                        }
                    }
                    if(thisData){
                        if(thisData.model){
                            var models = thisData.model.split(',');
                            var model=[];
                            for(var i=0,length=models.length;i<length;i++){
                                model.push({value:models[i],text:models[i]})
                            }
                            $('#model_combo').combobox('loadData',model);
                        }
                        if(thisData.type){
                            var types = thisData.type.split(',');
                            var type=[];
                            for(var i=0,length=types.length;i<length;i++){
                                type.push({value:types[i],text:types[i]})
                            }
                            console.log(type);
                             $('#type_combo').combobox('loadData',type);
                        }
                        $('#space_combo').combobox('reload','__ROOT__/index/game/getSpace?gid='+key);
                    }
                    }
                },
                onChange:function(key){
                    var data = $(this).combobox('getData');
                    var thisData;
                    $('#model_combo').combobox('setValue','');
                    $('#type_combo').combobox('setValue','');
                    $('#space_combo').combobox('setValue','');
                    for(var i=0,length=data.length;i<length;i++){
                        if(key==data[i].id){
                            thisData=data[i];
                            break;
                        }
                    }
                    if(thisData){
                        if(thisData.model){
                            var models = thisData.model.split(',');
                            var model=[];
                            for(var i=0,length=models.length;i<length;i++){
                                model.push({value:models[i],text:models[i]})
                            }
                            $('#model_combo').combobox('loadData',model);
                        }
                        if(thisData.type){
                            var types = thisData.type.split(',');
                            var type=[];
                            for(var i=0,length=types.length;i<length;i++){
                                type.push({value:types[i],text:types[i]})
                            }
                            console.log(type);
                             $('#type_combo').combobox('loadData',type);
                        }
                        $('#space_combo').combobox('reload','__ROOT__/index/game/getSpace?gid='+key);
                    }else{
                    $('#bt_rank_from').find('input[name=\'game_id\']').val('');
                    }
                }
                "></select>
            </td>
        </tr>
        <tr>
            <td>游戏模式：</td>
            <td><select id="model_combo" class="easyui-combobox" name="model" style="width:130px;"
                        data-options="
                panelHeight:'auto',
                editable:false,
                value:'<?php echo ($data["model"]); ?>'
                "></select>
            </td>
            <td>机体：</td>
            <td><select id="type_combo" class="easyui-combobox" name="type" style="width:130px;"
                        data-options="
                panelHeight:'auto',
                editable:false,
                value:'<?php echo ($data["type"]); ?>'
                "></select>
            </td>
        </tr>
        <tr>
            <td>平台：</td>
            <td><select id="space_combo" class="easyui-combobox easyui-validatebox" required="required" name="space_id" style="width:130px;"
                        data-options="
                valueField:'id',
                panelHeight:'auto',
                editable:false,
                value:'<?php echo ($data["space_id"]); ?>',
                textField:'name'
                "></select>
            </td>
            <td>到达关卡：</td>
            <td><input style="width: 130px;" type="text" name="stage" value="<?php echo ($data["stage"]); ?>"/>
            </td>
        </tr>
        <tr>
            <td>分数：</td>
            <td colspan="3"><input style="width: 348px;" type="text" name="score" class="easyui-validatebox" required="required"  value="<?php echo ($data["score"]); ?>"/>
            </td>
        </tr>
        <tr>
            <td>选择玩家：</td>
            <td colspan="3">
                <select id="gamer_combo" class="easyui-combobox" name="user_id" style="width:130px;"
                        data-options="
                 valueField:'id',
                panelHeight:'auto',
                value:'<?php echo ($data["user_id"]); ?>',
                textField:'bid',
                formatter:function(row){
                    return row.bid+'/'+row.name;
                },
                url:'__ROOT__/index/gamer/getDataNoPage',
                onChange:function(key){
                    var data = $(this).combobox('getData');
                    var thisData;
                    for(var i=0,length=data.length;i<length;i++){
                        if(key==data[i].id){
                            thisData=data[i];
                            break;
                        }
                    }
                    if(thisData){
                        $('#gamer_bid').val(thisData.bid);
                        $('#gamer_name').val(thisData.name);
                    }else{
                        $('#bt_rank_from').find('input[name=\'user_id\']').val('');
                    }
                }
                "></select>
            </td>
        </tr>
        <tr>
            <td>玩家机签：</td>
            <td><input id="gamer_bid" style="width: 130px;" type="text" name="user_bid" value="<?php echo ($data["user_bid"]); ?>"/>
            </td>
            <td>玩家姓名：</td>
            <td><input id="gamer_name" style="width: 130px;" type="text" name="user_name" value="<?php echo ($data["user_name"]); ?>"/>
            </td>
        </tr>
        <tr>
            <td>玩家所在地：</td>
            <td><input style="width: 130px;" type="text" name="loaction" value="<?php echo ($data["loaction"]); ?>"/>
            </td>
            <td>时间：</td>
            <td><input style="width: 130px;" type="text" name="time" class="easyui-datebox" value="<?php echo ($data["time"]); ?>"/>
            </td>
        </tr>
        <tr>
            <td>成绩资源：</td>
            <td colspan="3"><input style="width: 348px;" type="text" name="source"  value="<?php echo ($data["source"]); ?>"/>
            </td>
        </tr>
        <tr>
            <td>玩家留言：</td>
            <td colspan="3">
                <textarea name="comment" style="width: 348px;resize: none;height: 60px;"><?php echo ($data["comment"]); ?></textarea>
            </td>
        </tr>
    </table>
    <input type="hidden" name="id" value="<?php echo ($data["id"]); ?>">
</form>
<?php if (!defined('THINK_PATH')) exit();?><form id="bt_role_from" class="form">
    <table align="center">
        <tr>
            <td>游戏名称：</td>
            <td><input name="name" class="easyui-validatebox" required="required" type="text" value="<?php echo ($data["name"]); ?>" /></td>
        </tr>
        <tr>
            <td>英文名称：</td>
            <td><input name="name_en" type="text" value="<?php echo ($data["name_en"]); ?>"/> </td>
        </tr>
        <tr>
            <td>厂商：</td>
            <td><select id="bt_org_combo" class="easyui-combobox easyui-validatebox" required="required" name="brand_id" style="width:130px;"
                data-options="
                valueField:'id',
                editable:false,
                panelHeight:'auto',
                value:'<?php echo ($data["brand_id"]); ?>',
                textField:'name',
                url:'__ROOT__/index/brand/getData'
                "></select></td>
        </tr>
        <tr>
            <td>游戏模式：</td>
            <td><input name="model" type="text" value="<?php echo ($data["model"]); ?>"/> </td>
        </tr>
        <tr>
            <td>游戏机体：</td>
            <td><input name="type" type="text" value="<?php echo ($data["type"]); ?>"/> </td>
        </tr>
        <tr>
            <td>图片：</td>
            <td><input name="image" type="text" value="<?php echo ($data["image"]); ?>"/> </td>
        </tr>
        <tr>
            <td>资源：</td>
            <td><input name="source" type="text" value="<?php echo ($data["source"]); ?>"/> </td>
        </tr>
        <tr>
            <td>描述：</td>
            <td>
                <textarea name="description" style="width: 348px;resize: none;height: 60px;"><?php echo ($data["description"]); ?></textarea>
            </td>
        </tr>
        <tr>
            <td>平台：</td>
            <td><select id="space_combo" class="easyui-combobox easyui-validatebox" required="required" name="spaceIds" style="width:130px;"
                        data-options="
                valueField:'id',
                panelHeight:'auto',
                editable:false,
                multiple:true,
                value:[<?php echo ($dataSpace); ?>],
                textField:'name',
                url:'__ROOT__/index/space/getDataNoPage'
                "></select>
            </td>
        </tr>
        <tr>
            <td>排序：</td>
            <td><input name="rank" type="text" value="<?php echo ($data["rank"]); ?>"/> </td>
        </tr>
    </table>
    <input type="hidden" name="id" value="<?php echo ($data["id"]); ?>">
</form>
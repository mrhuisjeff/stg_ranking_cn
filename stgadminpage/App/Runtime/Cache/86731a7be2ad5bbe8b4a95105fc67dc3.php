<?php if (!defined('THINK_PATH')) exit(); if($isAjax): ?><!DOCTYPE html>
<html>
    <head>
        <title><?php echo ($title); ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="__ROOT____THM__/bootstrap/easyui.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="__ROOT____THM__/icon.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="__ROOT____CSS__/css.css" type="text/css" media="screen" />
        <script type="text/javascript" src="__ROOT____JS__/core/jquery-1.8.0.min.js"></script>
        <script type="text/javascript" src="__ROOT____JS__/core/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="__ROOT____JS__/locale/easyui-lang-zh_CN.js"></script>
        <script type="text/javascript" src="__ROOT____JS__/core/btutil.js"></script>
        <script type="text/javascript" src="__ROOT____JS__/My97DatePicker/WdatePicker.js"></script>
        <script>
            var _ROOT_ = '__ROOT__';
        </script>
    </head>
    <body>
        <div id="bt_loading" class="loading"></div>
        <div id="bt_loading_progress" class="progress">执行中...</div><?php endif; ?>
<table id="bt_dict_grid"></table>
<script type="text/javascript"> NameSpace("BT.dict", function() { var context = this; var $grid = $('#bt_dict_grid'), viewDialog, $typeGrid;
context.ready = function() {
    $grid.datagrid({
        fit: true,
        idField: 'did',
        url: _ROOT_ + '/index/space/getData',
        pagination: true,
        columns: [[
                {field: 'name', title: '平台名称', width: 90, align: 'center'},
                {field: 'description', title: '描述', width: 130, align: 'center'},
                {field: 'id', title: '操作', width: 100, align: 'center', formatter: function(value) {
                        return '<span title="编辑" class="img-btn icon-edit" type="update" id=' + value + '></span><span title="删除" class="img-btn icon-remove"  type="delete" id=' + value + '></span>';
                    }}
            ]],
        toolbar: [{
                text: '新增',
                iconCls: 'icon-add',
                handler: context.addView
            }],
        onLoadSuccess: function() {
            var $bodyView = $grid.data('datagrid').dc.view2;
            $bodyView.find('span[id]').click(function(e) {
                e.stopPropagation();
                var id = $(this).attr('id');
                var type = $(this).attr('type');
                if (type === 'update') {
                    context.updateView(id);
                } else {
                    context.doDelete(id);
                }
            });
        }
    });
};
context.addView = function() {
    viewDialog = $.dialog({
        title: '添加平台',
        href: _ROOT_ + '/index/space/toaddView',
        width: 450,
        bodyStyle: {overflow: 'hidden'},
        height: 250,
        buttons: [{
                text: '提交',
                handler: context.doSubmit
            }]
    });
};
context.updateView = function(id) {
    viewDialog = $.dialog({
        title: '更新平台',
        href: _ROOT_ + '/index/space/toupdateView?id=' + id,
        width: 450,
        bodyStyle: {overflow: 'hidden'},
        height: 250,
        buttons: [{
                text: '提交',
                handler: context.doSubmit
            }]
    });
};
context.doDelete = function(id) {
        $.confirm('确认删除？', function(r) {
            if (r) {
                $.post(_ROOT_ + '/index/space/doDelete', {id: id}, function(rsp) {
                    if (rsp.status) {
                        $grid.datagrid('reload');
                    } else {
                        $.alert(rsp.msg);
                    }
                }, 'JSON');
            }
        });
};

context.doSubmit = function() {
    var $bt_dict_from = $('#bt_dict_from');
    if ($bt_dict_from.form('validate')) {
        $.post(_ROOT_ + '/index/space/doSave', $bt_dict_from.toJson(), function(rsp) {
            if (rsp.status) {
                $grid.datagrid('reload');
                viewDialog.dialog('close');
            } else {
                $.alert(rsp.msg);
            }
        }, "JSON");
    }
};
 }); </script>
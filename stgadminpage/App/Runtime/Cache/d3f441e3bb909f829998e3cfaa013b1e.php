<?php if (!defined('THINK_PATH')) exit(); if($isAjax): ?><!DOCTYPE html>
<html>
    <head>
        <title><?php echo ($title); ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="__ROOT____THM__/bootstrap/easyui.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="__ROOT____THM__/icon.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="__ROOT____CSS__/css.css" type="text/css" media="screen" />
        <script type="text/javascript" src="__ROOT____JS__/core/jquery-1.8.0.min.js"></script>
        <script type="text/javascript" src="__ROOT____JS__/core/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="__ROOT____JS__/locale/easyui-lang-zh_CN.js"></script>
        <script type="text/javascript" src="__ROOT____JS__/core/btutil.js"></script>
        <script type="text/javascript" src="__ROOT____JS__/My97DatePicker/WdatePicker.js"></script>
        <script>
            var _ROOT_ = '__ROOT__';
        </script>
    </head>
    <body>
        <div id="bt_loading" class="loading"></div>
        <div id="bt_loading_progress" class="progress">执行中...</div><?php endif; ?>
<div class="easyui-layout" fit="true">
    <div region="north" style="height: 70px;border-bottom: none;">
        <form id="bt_user_search_from" onsubmit="return false;">
            <table style="height: 100%;">
                <tr>
                    <td>机签</td>
                    <td><input  type="text" name="Q_bid_like"/></td>
                    <td><a href="javascript:void(0)" class="easyui-linkbutton" id="bt_user_search_btn">查询</a> <a href="javascript:$('#bt_user_search_from').form('clear');" class="easyui-linkbutton">清空</a> </td>
                </tr>
            </table>
        </form>
    </div>
    <div region="center"><table id="bt_user_grid"></table></div>
</div>
<script type="text/javascript"> NameSpace("BT.gamer", function() { var context = this; var $grid = $('#bt_user_grid');
var viewDialog;

context.ready = function() {
    $grid.datagrid({
        fit: true,
        border: false,
        url: _ROOT_ + '/index/gamer/getData',
        pagination: true,
        columns: [[
                {field: 'name', title: '姓名', width: 100, align: 'center',formatter:html_encode},
                {field: 'bid', title: '机签', width: 100, align: 'center',formatter:html_encode},
                {field: 'description', title: '描述', width: 150,formatter:html_encode},
                {field: 'id', title: '操作', width: 100, align: 'center', formatter: function(value) {
                        return '<span title="编辑" class="img-btn icon-edit"  type="update"  id=' + value + '></span><span title="删除" class="img-btn icon-remove"  type="delete" id=' + value + '></span>';
                    }}
            ]],
        toolbar: [{
                text: '新增',
                iconCls: 'icon-add',
                handler: context.addView
            }],
        onLoadSuccess: function() {
            var $bodyView = $grid.data('datagrid').dc.view2;
            $bodyView.find('span[id]').click(function(e) {
                e.stopPropagation();
                var id = $(this).attr('id');
                var type = $(this).attr('type');
                if (type === 'update') {
                    context.updateView(id);
                } else {
                    context.doDelete(id);
                }
            });
        }
    });

    $('#bt_user_search_btn').click(function() {
        $grid.datagrid('load', $('#bt_user_search_from').toJson());
    });
};

context.addView = function() {
    viewDialog = $.dialog({
        title: '新增用户',
        href: _ROOT_ + '/index/gamer/toadd',
        width: 450,
        height: 250,
        bodyStyle: {overflow: 'hidden'},
        buttons: [{
                text: '提交',
                handler: context.doSubmit
            }]
    });
};

context.updateView = function(id) {
    viewDialog = $.dialog({
        title: '编辑用户',
        href: _ROOT_ + '/index/gamer/toUpdate?id=' + id,
        width: 450,
        height: 250,
        bodyStyle: {overflow: 'hidden'},
        buttons: [{
                text: '提交',
                handler: context.doSubmit
            }]
    });
};

context.doDelete = function(id) {
        $.confirm('确认删除？', function(r) {
            if (r) {
                $.post(_ROOT_ + '/index/gamer/doDelete', {id: id}, function(rsp) {
                    if (rsp.status) {
                        $grid.datagrid('reload');
                    } else {
                        $.alert(rsp.msg);
                    }
                }, 'JSON');
            }
        });
};

context.doSubmit = function() {
    var $bt_user_from = $('#bt_user_from');
    if ($bt_user_from.form('validate')) {
        $.post(_ROOT_ + '/index/gamer/doSave', $bt_user_from.toJson(), function(rsp) {
            if (rsp.status) {
                $grid.datagrid('reload');
                viewDialog.dialog('close');
            } else {
                $.alert(rsp.msg);
            }
        }, "JSON");
    }
}; }); </script>
<?php if (!defined('THINK_PATH')) exit(); if($isAjax): ?><!DOCTYPE html>
<html>
    <head>
        <title><?php echo ($title); ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="__ROOT____THM__/bootstrap/easyui.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="__ROOT____THM__/icon.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="__ROOT____CSS__/css.css" type="text/css" media="screen" />
        <script type="text/javascript" src="__ROOT____JS__/core/jquery-1.8.0.min.js"></script>
        <script type="text/javascript" src="__ROOT____JS__/core/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="__ROOT____JS__/locale/easyui-lang-zh_CN.js"></script>
        <script type="text/javascript" src="__ROOT____JS__/core/btutil.js"></script>
        <script type="text/javascript" src="__ROOT____JS__/My97DatePicker/WdatePicker.js"></script>
        <script>
            var _ROOT_ = '__ROOT__';
        </script>
    </head>
    <body>
        <div id="bt_loading" class="loading"></div>
        <div id="bt_loading_progress" class="progress">执行中...</div><?php endif; ?>
<div class="easyui-layout" fit="true">
    <div region="north" style="height: 70px;border-bottom: none;">
        <form id="bt_rank_search_from">
            <table style="height: 100%;">
                <tr>
                    <td>游戏：</td>
                    <td><select class="easyui-combobox" style="width: 130px;" data-options="
                valueField:'id',
                panelHeight:'auto',
                value:'<?php echo ($data["game_id"]); ?>',
                textField:'name',
                url:'__ROOT__/index/game/getDataNoPage',
                onChange:function(key){
                    var data = $(this).combobox('getData');
                    var thisData;
                    $('#model-search-combo').combobox('setValue','');
                    $('#type-search-combo').combobox('setValue','');
                    for(var i=0,length=data.length;i<length;i++){
                        if(key==data[i].id){
                            thisData=data[i];
                            break;
                        }
                    }
                    if(thisData){
                        if(thisData.model){
                            var models = thisData.model.split(',');
                            var model=[];
                            for(var i=0,length=models.length;i<length;i++){
                                model.push({value:models[i],text:models[i]})
                            }
                            $('#model-search-combo').combobox('loadData',model);
                        }
                        if(thisData.type){
                            var types = thisData.type.split(',');
                            var type=[];
                            for(var i=0,length=types.length;i<length;i++){
                                type.push({value:types[i],text:types[i]})
                            }
                            console.log(type);
                             $('#type-search-combo').combobox('loadData',type);
                        }
                        $('#space_combo').combobox('reload','__ROOT__/index/game/getSpace?gid='+key);
                    }
                }" name="game_id"></select></td>
                    <td>模式：</td>
                    <td><select id="model-search-combo" style="width: 130px;" class="easyui-combobox" data-options="panelHeight:'auto',editable:false" name="model"></select></td>
                    <td rowspan="2"><a href="javascript:void(0)" class="easyui-linkbutton" id="bt_rank_search_btn">查询</a> <a href="javascript:$('#bt_rank_search_from').find('.easyui-combobox').combobox('setValue','');" class="easyui-linkbutton">清空</a> </td>
                </tr>
                <tr>
                    <td>机体：</td>
                    <td><select style="width: 130px;" id="type-search-combo" class="easyui-combobox" data-options="panelHeight:'auto',editable:false" name="type"></select></td>
                    <td>状态</td>
                    <td><select class="easyui-combobox" data-options="panelHeight:'auto',editable:false" name="status"><option value="0">待审核</option><option value="1">审核通过</option><option value="2">审核未通过</option><option value="">全部</option></select></td>
                </tr>
            </table>
        </form>
    </div>
    <div region="center"><table id="bt_rank_grid"></table></div>
</div>
<script type="text/javascript"> NameSpace("BT.rank", function() { var context = this; var $grid = $('#bt_rank_grid');
var viewDialog;

context.ready = function() {
    var state = ["待审核","审核通过","审核未通过"];
    $grid.datagrid({
        fit: true,
        border: false,
        url: _ROOT_ + '/index/rank/getData',
        pagination: true,
        columns: [[
            {field: 'game_name', title: '游戏名称', width: 100,formatter:html_encode},
            {field: 'model', title: '模式', width: 100,formatter:html_encode},
            {field: 'type', title: '机型', width: 100,formatter:html_encode},
            {field: 'score', title: '分数', width: 100,formatter:html_encode},
            {field: 'stage', title: '到达位置', width: 100,formatter:html_encode},
            {field: 'user_bid', title: '玩家机签', width: 100,formatter:html_encode},
            {field: 'user_name', title: '玩家名', width: 100,formatter:html_encode},
            {field: 'comment', title: '玩家留言', width: 100,formatter:html_encode},
            {field: 'loaction', title: '玩家位置', width: 100,formatter:html_encode},
            {field: 'source', title: '成绩资源', width: 80,formatter: function(value) {
                return "<a href='"+value+"' target='_blank'>查看</a>";}},
            {field: 'space_name', title: '平台', width: 100,formatter:html_encode},
            {field: 'time', title: '时间', width: 150, align: 'center',formatter:html_encode},
            {field: 'creatTime', title: '创建时间', width: 150, align: 'center'},
            {field: 'status', title: '状态', width: 100, align: 'center', formatter: function(value) {
                return state[value];}},
            {field: 'id', title: '操作', width: 100, align: 'center', formatter: function(value) {
                return '<span title="编辑" class="img-btn icon-edit" type="update" id=' + value + '></span> <span title="删除" class="img-btn icon-remove"  type="delete" id=' + value + '></span>';
            }}
        ]],
        toolbar: [{
            text: '新增',
            iconCls: 'icon-add',
            handler: context.addView
        }],
        onLoadSuccess: function() {
            var $bodyView = $grid.data('datagrid').dc.view2;
            $bodyView.find('span[id]').click(function(e) {
                e.stopPropagation();
                var id = $(this).attr('id');
                var type = $(this).attr('type');
                if (type === 'update') {
                    context.updateView(id);
                } else {
                    context.doDelete(id);
                }
            });
        }
    });

    $('#bt_rank_search_btn').click(function() {
        $grid.datagrid('load', $('#bt_rank_search_from').toJson());
    });
};

context.addView = function() {
    viewDialog = $.dialog({
        title: '新增成绩',
        href: _ROOT_ + '/index/rank/toadd',
        width: 450,
        height: 450,
        bodyStyle: {overflow: 'hidden'},
        buttons: [{
            text: '提交',
            handler: context.doSubmit
        }]
    });
};

context.updateView = function(id) {
    viewDialog = $.dialog({
        title: '编辑成绩',
        href: _ROOT_ + '/index/rank/toUpdate?id=' + id,
        width: 450,
        height: 450,
        bodyStyle: {overflow: 'hidden'},
        buttons: [{
            text: '提交',
            handler: context.doSubmit
        }]
    });
};

context.doDelete = function(id) {
    $.confirm('确认删除？', function(r) {
        if (r) {
            $.post(_ROOT_ + '/index/rank/doDelete', {id: id}, function(rsp) {
                if (rsp.status) {
                    $grid.datagrid('reload');
                } else {
                    $.alert(rsp.msg);
                }
            }, 'JSON');
        }
    });
};

context.doSubmit = function() {
    var $bt_user_from = $('#bt_rank_from');
    if ($bt_user_from.form('validate')) {
        $.post(_ROOT_ + '/index/rank/doSave', $bt_user_from.toJson(), function(rsp) {
            if (rsp.status) {
                $grid.datagrid('reload');
                viewDialog.dialog('close');
            } else {
                $.alert(rsp.msg);
            }
        }, "JSON");
    }
}; }); </script>
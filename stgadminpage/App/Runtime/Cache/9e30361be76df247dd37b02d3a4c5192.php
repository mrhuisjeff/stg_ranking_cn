<?php if (!defined('THINK_PATH')) exit(); if($isAjax): ?><!DOCTYPE html>
<html>
    <head>
        <title><?php echo ($title); ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="__ROOT____THM__/bootstrap/easyui.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="__ROOT____THM__/icon.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="__ROOT____CSS__/css.css" type="text/css" media="screen" />
        <script type="text/javascript" src="__ROOT____JS__/core/jquery-1.8.0.min.js"></script>
        <script type="text/javascript" src="__ROOT____JS__/core/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="__ROOT____JS__/locale/easyui-lang-zh_CN.js"></script>
        <script type="text/javascript" src="__ROOT____JS__/core/btutil.js"></script>
        <script type="text/javascript" src="__ROOT____JS__/My97DatePicker/WdatePicker.js"></script>
        <script>
            var _ROOT_ = '__ROOT__';
        </script>
    </head>
    <body>
        <div id="bt_loading" class="loading"></div>
        <div id="bt_loading_progress" class="progress">执行中...</div><?php endif; ?>
<div class="easyui-layout" fit="true" id='bt_org_layout'>
    <div region="center" style="border-bottom: none;">
        <table id="bt_org"></table>
    </div>
    <!--<div region="east" style="width: 430px;padding: 2px;" title="部门成员" collapsed="true" split='true'>-->
        <!--<table id="bt_org_user_grid"></table>-->
    <!--</div>-->
</div>

<script type="text/javascript"> NameSpace("BT.brand", function() { var context = this; var $grid = $('#bt_org'), oid = 0, $userGrid = $('#bt_org_user_grid'), viewDialog, $chooseuserGrid;

context.ready = function() {
    $grid.treegrid({
        fit: true,
        border:false,
        url: _ROOT_ + '/index/brand/getData',
        idField: 'id',
        columns: [[
                {field: 'id', title: 'id', width: 40},
                {field: 'name', title: '厂商名称', width: 200},
                {field: 'description', title: '描述', width: 100},
                {field: 'rank', title: '序号', width: 50, align: 'center'},
                {field: 'source', title: '资源文件', width: 100, align: 'center'},
                {field: 'cid', title: '操作', width: 100, align: 'center', formatter: function(value) {
                        var ctrs = ['<span  title="编辑" class="img-btn icon-edit" type="update" oid=' + value + '></span>', '<span title="删除" class="img-btn icon-remove" type="delete" oid=' + value + '></span>'];
                        return ctrs.join(' ');
                    }}
            ]],
        toolbar: [{
                text: '新增',
                iconCls: 'icon-add',
                handler: context.addView
            }],
        onLoadSuccess: function() {
            var $bodyView = $grid.data('datagrid').dc.view2;
            $bodyView.find('span[oid]').click(function(e) {
                var type = $(this).attr('type');
                var oid = $(this).attr('oid');
                if (type === 'update') {
                    context.updateView(oid);
                } else {
                    context.deleted(oid);
                }
                e.stopPropagation();
            });
        }
    });
};
var viewDialog;
context.addView = function() {
    viewDialog = $.dialog({
        title: '新增菜单',
        href: _ROOT_ + '/index/brand/toadd',
        width: 450,
        bodyStyle: {overflow: 'hidden'},
        height: 250,
        buttons: [{
                text: '提交',
                handler: context.doSubmit
            }]
    });
};

context.deleted = function(id) {
    $.messager.confirm('提示', '确认删除？', function(r) {
        if (r) {
            $.post(_ROOT_ + '/index/brand/doDelete', {id: id}, function(rsp) {
                if (rsp.status) {
                    $grid.treegrid('remove', id);
                } else {
                    $.alert(rsp.msg);
                }
            });
        }
    });
};

context.doSubmit = function() {
    var $bt_org_from = $('#bt_org_from');
    if ($bt_org_from.form('validate')) {
        $.post(_ROOT_ + '/index/brand/doSave', $bt_org_from.toJson(), function(rsp) {
            if (rsp.status) {
                $grid.treegrid('reload');
                viewDialog.dialog('close');
            } else {
                $.alert(rsp.msg);
            }
        });
    }
};

context.updateView = function(id) {
    viewDialog = $.dialog({
        title: '更新菜单',
        href: _ROOT_ + '/index/brand/toUpdate?id=' + id,
        width: 430,
        bodyStyle: {overflow: 'hidden'},
        height: 250,
        buttons: [{
                text: '提交',
                handler: context.doSubmit
            }]
    });
};

 }); </script>